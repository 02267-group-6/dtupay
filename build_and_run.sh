#!/bin/bash
set -e

# Build and install the libraries
# abstracting away from using the
# RabbitMq message queue
pushd messaging-utilities
./build.sh
popd

pushd account-service
./build.sh
popd 

pushd payment-service
./build.sh
popd

pushd token-service
./build.sh
popd 

# Update the set of services and
# build and execute the system tests
pushd end_to_end_tests
./deploy.sh 
sleep 20s
./test.sh # to re-enable later
popd

# Cleanup the build images
docker image prune -f

