package models;

import models.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Customer model used in DTUPay
 *
 * @author Jeff Gyldenbrand, s202790
 */
public class Customer {

	private String id;
	private String name;
	private int cash;
	private ArrayList<String> tokens = new ArrayList<>();//it should be an arraylist of tokens as a customer can store up to 5 valid  tokens
	private String bankId;

	public Customer() {

	}

	public Customer(String id, String name, int cash, ArrayList<String> tokens, String bankId) {
		this.id = id;
		this.name = name;
		this.cash = cash;
		this.tokens = tokens;
		this.bankId = bankId;
	}

	/**
	 * Add a list of tokens to the customer's list of tokens.
	 *
	 * @param tokens a list of tokens
	 * @author Antoine Sébert s193508
	 */
	public void addTokens(List<String> tokens) {
		this.tokens.addAll(tokens);
	}

	/**
	 * Removes a token from the customer's list of tokens.
	 *
	 * @param token a token
	 */
	public void removeToken(String token) {
		for (String t : tokens)
			if (t.equals(token)) {
				tokens.remove(t);
				break;
			}
	}

	public int getTokenCount() {
		return tokens.size();
	}

	public void setTokenCount(ArrayList<String> tokens) {
		this.tokens = tokens;
	}

	public void printTokens() {
		System.out.println(name + "'s token ids:");
		for (String t : tokens) {
			System.out.println(t);
		}
	}

	public boolean hasToken(String token) {

		boolean foundToken = false;


		for (String t : tokens) {
			if (t.equals(token)) {
				foundToken = true;
			}
		}

		return foundToken;

	}

	public String getCustomer(String id) {
		return id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String id) {
		this.bankId = id;
	}


}
