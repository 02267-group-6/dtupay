package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

/**
 * Pay class is used for translating JSON content
 * from REST-endpoint to strings used in DTUPay
 *
 * @author Jeff Gyldenbrand, s202790
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pay", propOrder = {
		"amount",
		"token",
		"merchantId",

})


public class Pay {

	protected BigDecimal amount;
	protected String token;
	protected String merchantID;


	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal value) {
		this.amount = value;
	}

	public String getMerchantId() {
		return merchantID;
	}

	public void setMerchantId(String value) {
		this.merchantID = value;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String value) {
		this.token = value;
	}


}
