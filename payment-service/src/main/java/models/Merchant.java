package models;

/**
 * Merchant model used in DTUPay
 *
 * @author Jeff Gyldenbrand, s202790
 */
public class Merchant {

	private String id;//mid
	private String name;
	private int cash;// type can be double and amount
	private String bankId;

	public Merchant() {

	}

	public Merchant(String id, String name, int cash, String bankId) {
		this.id = id;
		this.name = name;
		this.cash = cash;
		this.bankId = bankId;
	}


	public String getMerchant(String id) {
		return id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}


	public String getBankId() {
		return bankId;
	}

	public void setBankId(String id) {
		this.bankId = id;
	}

}
