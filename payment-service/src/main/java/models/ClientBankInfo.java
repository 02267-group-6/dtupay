package models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * ClientBankInfo class is used for translating JSON content
 * from REST-endpoint to strings used in DTUPay
 *
 * @author Jeff Gyldenbrand, s202790
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClientBankInfo", propOrder = {
		"name",
		"bankId"

})
public class ClientBankInfo {

	protected String name;
	protected String bankId;

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String value) {
		this.bankId = value;
	}


}
