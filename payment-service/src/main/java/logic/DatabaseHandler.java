package logic;


import database.CustomerDB;
import database.MerchantDB;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import messaging.Event;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import messaging.EventReceiver;
import models.ClientBankInfo;
import models.Customer;
import models.Merchant;
import javax.ws.rs.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;



/**
 * Handles registrations, updates and deletions of customers and merchants
 * in the DTUPay database.
 *
 * @author Jeff Gyldenbrand, s202790
 */



public class DatabaseHandler implements EventReceiver {
	
	
	CustomerDB customerDatabase = CustomerDB.CustomerDB();
	MerchantDB merchantDatabase = MerchantDB.MerchantDB();
	BankService bank = new BankServiceService().getBankServicePort();


	private CompletableFuture<Integer> result;
	private EventSender eventSender;
	
	public DatabaseHandler(EventSender eventSender) {
        this.eventSender = eventSender;
   }
	

	/**
	 * Find a customer from its id.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Customer getCustomer(String id) {

		return customerDatabase.findCustomer(id);
	}

	/**
	 * Finds and returns a list of customers with the given name.
	 *
	 * @param name customer first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<Customer> getCustomerByName(String name) {
		System.out.println("getCustomerByName: " + name);
		return customerDatabase.findCustomerByName(name);
	}


	/**
	 * Registration of new customer.
	 * This function contacts the bank to retrieve the customers bank id.
	 *
	 * @param info consist of customers name and bank identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public int registerCustomer(ClientBankInfo info) {

		try {
			Customer c = new Customer();
			c.setName(info.getName());
			c.setBankId(info.getBankId());


			customerDatabase.insertCustomerToDatabase(c);
			return Integer.parseInt(CustomerDB.db.get(CustomerDB.db.size() - 1).getId());

		} catch (Exception e) {
			throw new NotFoundException(e);
		}

	}


	/**
	 * Updates existing customers information
	 *
	 * @param id       customers DTUPay identification.
	 * @param customer customer object.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Customer updateCustomer(String id, Customer customer) {
		customerDatabase.updateCustomer(id, customer);
		return CustomerDB.db.get(Integer.parseInt(id));
	}


	/**
	 * Removes a customer from the database.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void unregisterCustomer(String id) {
		customerDatabase.unregisterCustomer(id);
	}


	/**
	 * Finds a merchant from its id.
	 *
	 * @param id merchant DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Merchant getMerchant(String id) {

		return merchantDatabase.findMerchant(id);
	}


	/**
	 * Registration of new merchant.
	 * This function contacts the bank to retrieve the merchants bank id.
	 *
	 * @param info consist of merchants name and bank identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public int registerMerchant(ClientBankInfo info) {

		try {
			Merchant m = new Merchant();
			m.setName(info.getName());
			m.setBankId(info.getBankId());

			merchantDatabase.insertMerchantToDatabase(m);

			return Integer.parseInt(MerchantDB.db.get(MerchantDB.db.size() - 1).getId());

		} catch (Exception e) {
			throw new NotFoundException(e);
		}

	}

	/**
	 * Finds and returns a list of merchants with the given name.
	 *
	 * @param name merchants first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<Merchant> getMerchantByName(String name) {
		return merchantDatabase.findMerchantByName(name);
	}


	/**
	 * Finds and returns a list of merchants with the given name.
	 *
	 * @param name merchants first name.
	 * @author Jeff Gyldenbrand, s202790, Daniil Provornii s192678
	 */
	@Override
	public void receiveEvent(Event event) {
	    switch(event.getEventType()) {
	        case "create_customer":
	        
	        	ClientBankInfo info = new ClientBankInfo();
	        	
	        	info.setName(String.valueOf(event.getArguments()[0]));
	        	info.setBankId(String.valueOf(event.getArguments()[1]));
	        	
	        	int cId = registerCustomer(info);
	        	
	        	
	            System.out.println("create_customer: " + event);
	            try {
	        		eventSender.sendEvent(new Event("user_created", new String[] {Integer.toString(cId)}));
	        	} catch (Exception e) {
					System.err.println("Could not send tokencountanswer");
				}
	            break;
	            
	        case "create_merchant":
	        
	        	ClientBankInfo merchantInfo = new ClientBankInfo();
	        	
	        	merchantInfo.setName(String.valueOf(event.getArguments()[0]));
	        	merchantInfo.setBankId(String.valueOf(event.getArguments()[1]));
	        	
	        	int mId = registerMerchant(merchantInfo);
	        	
	            System.out.println("create_merchant: " + event);
	            try {
	        		eventSender.sendEvent(new Event("user_created", new String[] {Integer.toString(mId)}));
	        	} catch (Exception e) {
					System.err.println("Could not send tokencountanswer");
				}
	            break;
	            
	        case "tokenCountRequest":
	            System.out.println("tokenCountRequest event ");

	        	String customerId = String.valueOf(event.getArguments()[0]);
	        	Integer tokenCount;
	        	try {
	        		Customer customer = CustomerDB.CustomerDB().findCustomer(customerId);
	        		tokenCount= customer.getTokenCount();
	        	} catch (Exception e) {
	        		tokenCount = -1;
	        	}
	        	
	        	try {
	        		eventSender.sendEvent(new Event("tokenCountAnswer", new String[] {Integer.toString(tokenCount)}));
	        	} catch (Exception e) {
					System.err.println("Could not send tokencountanswer");
				}
	        	break;
	        	
	        case "tokenAdd":
	            System.out.println("tokenAdd event ");

	        	String listAsString = String.valueOf(event.getArguments()[0]);
	        	String trimmedList = listAsString.substring(1, listAsString.length() - 1);
	        	String[] splitted = trimmedList.split(",");
	        	
	        	String customerID = splitted[0].trim();
	        	List<String> tokenList = new ArrayList<>();
	        	
	        	for (int i = 1; i < splitted.length; i++) {
	        		tokenList.add(splitted[i].trim());
	        	}
	        	
	        	
        		Customer customer = CustomerDB.CustomerDB().findCustomer(customerID);
        		customer.addTokens(tokenList);
        		try {
	        		eventSender.sendEvent(new Event("tokenAdded"));
	        	} catch (Exception e) {
					System.err.println("Could not send tokenAdded");
				}
	        	
	        default:
	            System.out.println("event ignored: " + event);
	    }
	}



}
