package resources.payment;

import logic.BankHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Endpoint:
 * POST: makes a Transaction between customer and merchant.
 * returns error/success message as a string.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("/pay")
public class Pay {
	BankHandler accountHandler = new BankHandler();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response makePay(models.Pay pay) {
		try {
			String ret = accountHandler.pay(pay.getToken(), pay.getMerchantId(), pay.getAmount());

			return Response.ok(ret, MediaType.TEXT_PLAIN).build();

		} catch (Exception e) {
			throw new Error("Response ... Post: " + e.getMessage());
		}
	}
}
