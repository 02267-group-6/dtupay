package resources.account;

import dtu.ws.fastmoney.AccountInfo;
import logic.BankHandler;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Endpoint:
 * GET: gets a list of all bank accounts.
 * POST: creates a bank account given provided user and balance.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("accounts")
public class Accounts {

	BankHandler bank = new BankHandler();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AccountInfo> getBankAccounts() {
		return bank.getBankAccounts();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerCustomer(dtu.ws.fastmoney.Account account) {

		String id = bank.createAccount(account.getUser(), account.getBalance());
		return Response.ok(id, MediaType.TEXT_PLAIN).build();
	}


}
