# author: Christian Bruun Nielsen, s153867, and Jeff Gyldenbrand, s202790
#
# The first scenario depends on the customer already having a bank account.

Feature: Create, update and delete users in DTUPay

	# creating a customer
	Scenario: Create a customer at DTUPay
		Given the customer has a bank account id "221190-1355"
		When customer creates the DTUPay account
		Then the response is the account id is "0", the bank id is "42ec4b47-bee2-45a9-9ab5-eb55531401ee" and the name is "Jeff"

	# Updating a user's name
	Scenario: Update the customer's name
		Given the user's id is "0" and updating the name to "Chris"
		When searching on the user by his id "0"
		Then the name "Chris" is returned
		
	# Deleting a user
	Scenario: Delete the user
		When deleting the user with id "0"
		Then on search for the user with id "0" the return error should be "deleted user"

	# Print users
	Scenario: Print users for an empty and then populated database
		Given the count is "0"
		When adding two new users
		Then the count is now "2"
