package token_service.logic;

/**
 * Upon a new tokens request, when a Customer already has more than one unused token.
 *
 * @author Antoine Sébert s193508
 */
public class TooManyUnusedTokens extends Exception {
	public TooManyUnusedTokens(String message) {
		super(message);
	}
}
