package token_service.logic;

import messaging.EventReceiver;
import messaging.EventSender;
import token_service.model.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import messaging.Event;

/**
 * Token Manager providing unused tokens.
 *
 * @author Antoine Sébert s193508
 */
public class TokenManager implements EventReceiver {
	private EventSender eventSender;
	private CompletableFuture<Integer> resultCount;
	private CompletableFuture<Boolean> resultAdd;
	private final static int maxTokenAmountRequest = 5;

	public TokenManager(EventSender eventSender) {
		this.eventSender = eventSender;
	}

	@Override
	public void receiveEvent(Event event) {
		switch(event.getEventType()) {
			case "tokenAdded":
				System.out.println("tokenAdded: " + event);
				resultAdd.complete(true);
				
				break;
			case "tokenCountAnswer":
				System.out.println("tokenCountAnswer: " + event);
				resultCount.complete(Integer.parseInt(String.valueOf(event.getArguments()[0])));

				break;
			default:
				System.out.println("event ignored: " + event);
		}
	}

	/**
	 * Checks the request, eventually creates new tokens and add them to the database.
	 *
	 * @param id     a Customer ID
	 * @param amount the amount of requested tokens
	 * @return a list of unused tokens of size `amount`
	 * @throws TooManyUnusedTokens if the Customer has more than 1 unused tokens
	 * @throws CustomerNotFound    if the customer id does not exists
	 * @throws InvalidTokenAmount  if the amount requested is lower than `1` or greater than
	 *                             <code>maxTokenAmountRequest</code>
	 */
	public List<Token> request(String id, int amount) throws InvalidTokenAmount, TooManyUnusedTokens {
		System.out.println("Amount is " + Integer.toString(amount));

		if (0 < amount && amount <= TokenManager.maxTokenAmountRequest)
			try {
				int count = getTokenCount(id);
				System.out.println("Count for the id " +id + " is " + Integer.toString(count));
				if (0 <= count && count <= 1) {
					List<Token> tokens = new ArrayList<>();
					List<String> args = new ArrayList<String>( Arrays.asList(id));

					for (int i = 0; i < amount; i++) {
						Token token = new Token();
						tokens.add(token);
						args.add(token.getId().toString());
					}

					Event event = new Event("tokenAdd", new List[]{args});
					resultAdd = new CompletableFuture<>();
					eventSender.sendEvent(event);
					resultAdd.join();

					return tokens;
				} else
					throw new TooManyUnusedTokens("Too many unused tokens : " + count);
			} catch (TooManyUnusedTokens e) {
				throw e; // rethrow
			} catch (Exception e) {
				e.printStackTrace();
			}
		else
			throw new InvalidTokenAmount("Invalid token amount : " + amount);

		return null;
	}

	/**
	 * Retrieves the number of unused tokens a Customer has.
	 *
	 * @param id a Customer ID
	 * @return the number of unused tokens
	 * @throws CustomerNotFound if the customer id does not exists
	 */
	public int getTokenCount(String id) throws Exception {
		Event event = new Event("tokenCountRequest", new String[]{id});

		resultCount = new CompletableFuture<>();
		eventSender.sendEvent(event);

		int count = resultCount.join();

		if (count == -1)
			throw new CustomerNotFound("Customer not found : " + id);

		return count;
	}
}
