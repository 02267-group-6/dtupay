package token_service.logic;

/**
 * When a Customer is not present in the database.
 *
 * @author Antoine Sébert s193508
 */
public class CustomerNotFound extends Exception {
	public CustomerNotFound(String message) {
		super(message);
	}
}
