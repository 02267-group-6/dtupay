package token_service.logic;

/**
 * Upon a new tokens request, when a Customer does not request [1; 5] tokens.
 *
 * @author Antoine Sébert s193508
 */
public class InvalidTokenAmount extends Exception {
	public InvalidTokenAmount(String message) {
		super(message);
	}
}
