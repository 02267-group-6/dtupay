package token_service.resources;

import token_service.logic.CustomerNotFound;
import token_service.logic.InvalidTokenAmount;
import token_service.logic.TokenManager;
import token_service.logic.TooManyUnusedTokens;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Endpoints for the token microservice.
 *
 * @author Antoine Sébert s193508
 * @version 0.1.0
 */
@Path("/token")
public class Token {
	static TokenManager service = new TokenManagerFactory().getService();

	/**
	 * Base endpoint.
	 *
	 * @return No Content
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response base() {
		return Response.noContent().build();
	}

	/**
	 * Endpoint for unused tokens amount queries.
	 *
	 * @param id a Customer ID
	 * @return OK containing the number of unused tokens, or 404 if the customer does not exist
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getTokenCount(@PathParam("id") String id) {
		try {
			return Response.ok(service.getTokenCount(id), MediaType.TEXT_PLAIN).build();
		} catch (Exception e) {
			return Response.status(404).entity(e.getMessage()).build();
		}
	}

	/**
	 * Endpoint for new unused tokens requests.
	 *
	 * @param id     a Customer ID
	 * @param amount the amount of requested tokens
	 * @return OK with the new unused tokens, 400 if the requested amount is invalid,
	 * 404 if the customer does not exist, or 428 if the customer has too many unused tokens
	 */
	@GET
	@Path("/request/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response request(@PathParam("id") String id, @QueryParam("amount") int amount) {
		try {
			System.out.println("REST Count for the id " +id + " is " + Integer.toString(amount));

			List<token_service.model.Token> tokens = service.request(id, amount);

			if (tokens.size() == 1)
				return Response.ok("{\"tokens\": [\"" + tokens.get(0) + "\"]}", MediaType.APPLICATION_JSON).build();
			else {
				String payload = "{\"tokens\": [";

				for (int i = 0; i < tokens.size() - 1; i++)
					payload += "\"" + tokens.get(i).getId() + "\",";

				payload += "\"" + tokens.get(tokens.size() - 1).getId() + "\"]}";

				return Response.ok(payload, MediaType.APPLICATION_JSON).build();
			}
		} catch (InvalidTokenAmount e) {
			return Response.status(400).entity(e.getMessage()).build();
			/*
		} catch (CustomerNotFound e) {
			return Response.status(404).entity(e.getMessage()).build();
			
			 */
		} catch (TooManyUnusedTokens e) {
			return Response.status(428).entity(e.getMessage()).build();
		}
	}
}
