package token_service.model;

import java.util.UUID;

/**
 * Token class relying on UUID.
 *
 * @author Antoine Sébert s193508
 * @version 1.0.0
 */
public class Token {
	private UUID id;
	private long timestamp;

	/**
	 * Creates a new Token associated with a Customer.
	 */
	public Token() {
		id = UUID.randomUUID();
		timestamp = System.currentTimeMillis();
	}

	/**
	 * Returns the token's ID.
	 *
	 * @return The token's unique ID
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * Returns the token's timestamp.
	 *
	 * @return the token timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	public String toJSON() {
		return "{id : " + id + ",timestamp:" + timestamp + "}";
	}
}
