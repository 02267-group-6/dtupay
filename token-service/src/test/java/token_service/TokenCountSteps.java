package token_service;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;
import token_service.logic.TokenManager;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Steps for the token count test.
 *
 * @author Antoine Sébert s193508
 */
public class TokenCountSteps {
	TokenManager s;
	Event event;
	CompletableFuture<Integer> result = new CompletableFuture<>();
	String requested;
	int amount = 0;

	public TokenCountSteps() {
		s = new TokenManager(new EventSender() {
			@Override
			public void sendEvent(Event ev) throws Exception {
				event = ev;
			}
		});
	}

	@Given("a customer with id {string} and {int} tokens")
	public void aCustomerWithIdAndTokens(String arg0, int arg1) {
		amount = arg1;
	}

	@When("I request a token count for a customer {string}")
	public void iRequestATokenCountForACustomer(String id) {
		requested = id;

		new Thread(() -> {
			try {
				result.complete(s.getTokenCount(id));
			} catch (Exception e) {
				throw new Error(e);
			}
		}).start();
	}

	@Then("a {string} is sent")
	public void aIsSent(String eventType) {
		assertEquals(eventType, event.getEventType());
	}

	@When("a {string} event is received")
	public void aEventIsReceived(String eventType) {
		s.receiveEvent(new Event(eventType, new Object[]{amount}));
	}

	@Then("the token count is {int}.")
	public void theTokenCountIs(int arg1) {
		assertEquals(arg1, result.join());
	}
}
