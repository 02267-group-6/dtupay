package token_service;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;
import token_service.logic.CustomerNotFound;
import token_service.logic.InvalidTokenAmount;
import token_service.logic.TokenManager;
import token_service.logic.TooManyUnusedTokens;
import token_service.model.Token;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Steps for the token request test.
 *
 * @author Antoine Sébert s193508
 */
public class TokenRequestSteps {
	/*
	//private static TokenManager manager = TokenManager.TokenManager();
	private static CustomerDB db = CustomerDB.CustomerDB();
	private static HashMap<String, Integer> requests = new HashMap(3);

	@Given("the customer with id {string} exists and has {int} tokens")
	public void theCustomerWithIdExistsAndHasTokens(String id, int count) {
		ArrayList<Token> tokens = new ArrayList<Token>();

		for (int i = 0; i < count; i++)
			tokens.add(new Token());

		//db.insertCustomerToDatabase(new Customer(id, "testCustomer" + id, 0, tokens, "0"));
	}

	@And("the customer {string} requests {int} tokens")
	public void theCustomerRequestsTokens(String id, int count) {
		try {
			requests.put(id, count);
			manager.request(id, count);
		} catch (InvalidTokenAmount | CustomerNotFound | TooManyUnusedTokens invalidTokenAmount) {
		}
	}

	@Then("the customer {string} has {int} tokens.")
	public void theCustomerHasTokens(String id, int count) {
		Assertions.assertEquals(count, db.tokenCount(id));
	}

	@Then("the token request for the customer {string} produces a TooManyUnusedTokens exception.")
	public void theTokenRequestForTheCustomerProducesATooManyUnusedTokensException(String id) {
		Assertions.assertThrows(TooManyUnusedTokens.class, () -> {
			manager.request(id, requests.get(id));
		});
	}

	@Then("the token request for the customer {string} produces a InvalidTokenAmount exception.")
	public void theTokenRequestForTheCustomerProducesAInvalidTokenAmountException(String id) {
		Assertions.assertThrows(InvalidTokenAmount.class, () -> {
			manager.request(id, requests.get(id));
		});
	}

	@Given("the customer with id {string} does not exists")
	public void theCustomerWithIdDoesNotExists(String id) {
		Assertions.assertFalse(db.isCustomerValid(id));
	}

	@Then("the token request for the customer {string} produces a CustomerNotFound exception.")
	public void theTokenRequestForTheCustomerProducesACustomerNotFoundException(String id) {
		Assertions.assertThrows(CustomerNotFound.class, () -> {
			manager.request(id, requests.get(id));
		});
	}
	*/
}
