# author: Antoine Sébert s193508

Feature: Token Manager get count

	Scenario: get the token count for an existing customer
		Given a customer with id "0" and 4 tokens
		When I request a token count for a customer "0"
		Then a "tokenCountRequest" is sent
		When a "tokenCountAnswer" event is received
		Then the token count is 4.