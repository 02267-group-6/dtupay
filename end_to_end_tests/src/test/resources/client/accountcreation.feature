# author Daniil Provornii, s192678
Feature: AccountCreation
	The new user should be able to create an account as customer or merchant
	
	# Success scenarios
	# ----------------------------------------------------	
	
	# Register user as customer
	Scenario: Customer registration
		Given user with name "An", CPR "111195-4679", balance 500
		When registering as customer
		Then user id is received
		
		# Register user as merchant
	Scenario: Merchant registration
		Given user with name "An", CPR "111195-4679", balance 500
		When registering as merchant
		Then user id is received