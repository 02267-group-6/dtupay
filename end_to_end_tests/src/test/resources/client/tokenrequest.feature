# author Daniil Provornii, s192678, Ergys Kajo s181412, Hussain Tariq Awana s181434
Feature: TokenRequest
	The customer should be able to request at least a token
	
	# Success scenarios
	# ----------------------------------------------------	
	
	# Request one token (when smaller than six in total)
	Scenario: One token valid request
		Given customer with name "An", CPR "111195-4679", balance 500
		When requesting 1 tokens
		Then received 1 tokens
		
	# Request more tokens (when smaller than six in total)
	Scenario: Many tokens valid request
		Given customer with name "An", CPR "111195-4679", balance 500
		When requesting 5 tokens
		Then received 5 tokens