# author Daniil Provornii, s192678
Feature: Payment
	The merchant should be able to initiate payment,
											the money should be transferred
	

  Scenario: Successful Payment
    Given a merchant with name "An", CPR "111195-4679", balance 500
    And a customer with name "Frederi", CPR "121296-1359", balance 700
    When merchant requests a payment for 10 kr from customer
    Then success message response
    And customer balance is 690
    And merchant balance is 510

	# Failed payment scenarios	
  Scenario: Lacking money
    Given a merchant with name "An", CPR "111195-4679", balance 500
    And a customer with name "Frederi", CPR "121296-1359", balance 700
    When merchant requests a payment for 1000 kr from customer
    Then not enough money error returned