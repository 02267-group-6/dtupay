package client;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import logic.BankHandler;

/**
 * Defines the logic for customer/merchant creation scenarios
 *
 * @author Daniil Provornii, s192678
 */
public class AccountCreationSteps {
	CustomerAPI customerAPI = new CustomerAPI();
	MerchantAPI merchantAPI = new MerchantAPI();
	BankHandler bankHandler = new BankHandler();
	
	String userName = "";
	String userBankID = "";
	String userType = "";
	
	String dtyPayId = "";
	
	@Given("user with name {string}, CPR {string}, balance {int}")
	public void userDetails(String name, String cpr, int balance) {
		userBankID = bankHandler.createAccount(name, cpr, new BigDecimal(balance));
		userName = name;
	}
	
	@When("registering as customer")
	public void registerAsCustomer() {
		dtyPayId = customerAPI.registerCustomer(userName, userBankID);
	}

	@When("registering as merchant")
	public void registerAsMerchant() {
		dtyPayId = merchantAPI.registerMerchant(userName, userBankID);
	}
		
	@Then("user id is received")
	public void userIdReceived() {
		assertTrue(dtyPayId.matches(".*\\d.*"));
	}
	
	@After
	public void cleanUp() {
		if (bankHandler.accountExists(userBankID)) {
			bankHandler.retire(userBankID);
		}
		customerAPI.closeConnection();
		merchantAPI.closeConnection();
	}
}
