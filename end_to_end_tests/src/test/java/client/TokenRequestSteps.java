package client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import logic.BankHandler;

/**
 * Defines the logic for token request scenarios
 *
 * @author Daniil Provornii, s192678, Ergys Kajo s181412, Hussain Tariq Awana s181434
 */
public class TokenRequestSteps {
	CustomerAPI customerAPI = new CustomerAPI();
	BankHandler bankHandler = new BankHandler();
	
	String customerName = "";
	String customerBankID = "";
	String[] tokens = new String[0];
	
	@Given("customer with name {string}, CPR {string}, balance {int}")
	public void customerDetails(String name, String cpr, int balance) {
		customerBankID = bankHandler.createAccount(name, cpr, new BigDecimal(balance));
		customerName = name;
	}
	
	@When("requesting {int} tokens")
	public void requestTokens(int amount) {
		String customerID = customerAPI.registerCustomer(customerName, customerBankID);
		tokens = customerAPI.getTokensForCustomer(amount, customerID);
		
	}

	@Then("received {int} tokens")
	public void receivedTokens(int amount) {
		assertEquals(amount, tokens.length);
	}
	
	@After
	public void cleanUp() {
		if (bankHandler.accountExists(customerBankID)) {
			bankHandler.retire(customerBankID);
		}
		customerAPI.closeConnection();
	}
}
