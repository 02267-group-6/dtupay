package client;

import io.cucumber.junit.platform.engine.Cucumber;

/**
 * Runs the cucumber tests
 *
 * @author Daniil Provornii, s192678
 */
@Cucumber
public class ClientTest {
}