package client;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import logic.BankHandler;

/**
 * Defines the logic for payment scenarios
 *
 * @author Daniil Provornii, s192678
 */
public class PaymentSteps {
	CustomerAPI customerAPI = new CustomerAPI();
	MerchantAPI merchantAPI = new MerchantAPI();
	BankHandler bankHandler = new BankHandler();
	
	String merchantName = "";
	String merchantBankID = "";
	String customerName = "";
	String customerBankID = "";
	String paymentMessage = "";
	
	@Given("a merchant with name {string}, CPR {string}, balance {int}")
	public void merchantWithParams(String name, String cpr, int balance) {
		merchantBankID = bankHandler.createAccount(name, cpr, new BigDecimal(balance));
		merchantName = name;
	}
	
	@Given("a customer with name {string}, CPR {string}, balance {int}")
	public void customerWithParams(String name, String cpr, int balance) {
		customerBankID = bankHandler.createAccount(name, cpr, new BigDecimal(balance));
		customerName = name;
	}
	
	@When("merchant requests a payment for {int} kr from customer")
	public void initiatePayment(int amount) {
		String customerID = customerAPI.registerCustomer(customerName, customerBankID);
		String merchantID = merchantAPI.registerMerchant(merchantName, merchantBankID);
		
		String[] tokens = customerAPI.getTokensForCustomer(5, customerID);
		String token = tokens[0];
		
		paymentMessage = merchantAPI.pay(token, merchantID, amount);
	}

	@Then("success message response")
	public void isSuccessMessage() {
		assertEquals(paymentMessage, "SUCCESS");
	}
	
	@Then("customer balance is {int}")
	public void customerBalanceIs(int amount) {
		BigDecimal balance = bankHandler.getBalance(customerBankID);
		BigDecimal decimalAmount = new BigDecimal(amount);
		
		assertEquals(balance, decimalAmount);
	}
	
	@Then("merchant balance is {int}")
	public void merchantBalanceIs(int amount) {
		BigDecimal balance = bankHandler.getBalance(merchantBankID);
		BigDecimal decimalAmount = new BigDecimal(amount);
		
		assertEquals(balance, decimalAmount);
	}
	
	@Then("not enough money error returned")
	public void nonNegativeReturned() {
	    assertEquals(paymentMessage, "LACK MONEY");
	}
	
	@After
	public void cleanUp() {
		if (bankHandler.accountExists(customerBankID)) {
			bankHandler.retire(customerBankID);
		}
		if (bankHandler.accountExists(merchantBankID)) {
			bankHandler.retire(merchantBankID);
		}
		customerAPI.closeConnection();
		merchantAPI.closeConnection();
	}
}
