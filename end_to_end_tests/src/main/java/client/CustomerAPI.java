package client;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import models.ClientBankInfo;
import models.TokenResponse;

/**
 * Simulates a customer requesting DTUPay adapters
 *
 * @author Daniil Provornii, s192678
 */
public class CustomerAPI {
	Client client;
	WebTarget userUrl;
	WebTarget tokenUrl;
	
	/**
	 * Initialize the client targets for various scenarios 
	 *
	 * @author Daniil Provornii,  s192678
	 */
	public CustomerAPI() {
		client = ClientBuilder.newClient();
		
		userUrl = client.target("http://localhost:8083/");
		tokenUrl = client.target("http://localhost:8082/");
	}
	
	/**
	 * Creates a customer account in DTUPay system
	 *
	 * @param name		 user name
	 * @param bankId	 bank account id in the bank service
	 * @return 			 id of the created user in DTUPay
	 * @author Daniil Provornii, s192678
	 */
	public String registerCustomer(String name, String bankId) {
		
		ClientBankInfo clientBankInfo = new ClientBankInfo();
		clientBankInfo.setName(name);
		clientBankInfo.setBankId(bankId);
		clientBankInfo.setUserType("customer");
		Response response = userUrl.path("users")
								   .request()
								   .post(Entity.json(clientBankInfo));
		String body = response.readEntity(String.class);
		response.close();
		
		return body;
	}
	
	/**
	 * Requests tokens for customer according to the ID
	 *
	 * @param tokensAmount		number of tokens requested
	 * @param customerID		id of the customer in DTUPay
	 * @return 					array of tokens (as strings) of size tokensAmount
	 * @author Daniil Provornii, s192678, Ergys Kajo s181412, Hussain Tariq Awana s181434
	 */
	public String[] getTokensForCustomer(int tokensAmount, String customerID) {
		
		Response response = tokenUrl.path("token/request/"+customerID)
								   .queryParam("amount", tokensAmount)
								   .request()
								   .get();
		
		TokenResponse tokenResponse  = response.readEntity(TokenResponse.class);
		String[] tokens = tokenResponse.getTokens();
		response.close();
		
		return tokens;
	}
	
	public void closeConnection() {
		client.close();
	}

}
