package client;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import models.ClientBankInfo;
import models.Pay;

/**
 * Simulates a merchant requesting DTUPay adapters
 *
 * @author Daniil Provornii, s192678
 */
public class MerchantAPI {
	Client client;
	WebTarget userUrl;
	WebTarget paymentUrl;

	/**
	 * Initialize the payment target for the client request
	 *
	 * @author Daniil Provornii,  s192678
	 */
	public MerchantAPI() {
		client = ClientBuilder.newClient();
		
		userUrl = client.target("http://localhost:8083/");
		paymentUrl = client.target("http://localhost:8081/");
	}
	
	/**
	 * Creates a merchant account in DTUPay system
	 *
	 * @param name		 user name
	 * @param bankId	 bank account id in the bank service
	 * @return 			 id of the created user in DTUPay
	 * @author Daniil Provornii, s192678
	 */
	public String registerMerchant(String name, String bankId) {
		ClientBankInfo clientBankInfo = new ClientBankInfo();
		clientBankInfo.setName(name);
		clientBankInfo.setBankId(bankId);
		clientBankInfo.setUserType("merchant");
		Response response = userUrl.path("users")
								   .request()
								   .post(Entity.json(clientBankInfo));
		String body = response.readEntity(String.class);
		response.close();
		return body;
	}
	
	/**
	 * Requests a payment from customer to merchant
	 *
	 * @param token		 a valid token generated for the customer
	 * @param bankId	 bank account id in the bank service
	 * @return 			 message received from the server about the payment result
	 * @author Daniil Provornii, s192678
	 */
	public String pay(String token, String merchantID, int amount) {
		Pay pay = new Pay();
		pay.setAmount(new BigDecimal(amount));
		pay.setToken(token);
		pay.setMerchantId(merchantID);
		
		Response response = paymentUrl.path("pay")
								   .request()
								   .post(Entity.json(pay));
		int status = response.getStatus();
		
		if (status != 200) {
			response.close();
			return "FAILURE";
		}
		
		String body = response.readEntity(String.class);
		response.close();
		
		if (body.contains("not enough money")) {
			return "LACK MONEY";
		}
		
		return "SUCCESS";
	}
	
	public void closeConnection() {
		client.close();
	}
}
