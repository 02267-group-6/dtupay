package logic;

import java.math.BigDecimal;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

/**
 * Coordinates the connection to the bank and specific manipulations
 *
 * @author Daniil Provornii, s192678
 */
public class BankHandler {
	BankService bank = new BankServiceService().getBankServicePort();
	
	/**
	 * Creates an account in the bank system
	 *
	 * @param name		 user name
	 * @param CPR		 user CPR
	 * @param balance	 amount of money on balance
	 * @return 			 bankAccountId if created and
	 * 					 empty string (and print error) otherwise
	 * @author Daniil Provornii, s192678
	 */
	public String createAccount(String name, String CPR, BigDecimal balance) {
		User user = new User();
		user.setFirstName(name);
		user.setLastName("Doe");
		user.setCprNumber(CPR);
		
		try {
			return bank.createAccountWithBalance(user, balance);
			
		} catch (BankServiceException_Exception e) {
			System.err.println("createAccount: " + e.getMessage());
			return "";
		}
	}
	
	/**
	 * Retires an account from bank system
	 *
	 * @param accountID	 id of the user in the bank application
	 *
	 * @author Daniil Provornii, s192678
	 */
	public void retire(String acccountID) {
		try {
			bank.retireAccount(acccountID);
		} catch (BankServiceException_Exception e) {
	
			System.err.println("retireAccount: " + e.getMessage());
		}
	}
	
	/**
	 * Get the current balance of user in the bank
	 *
	 * @param accountID	 id of the user in the bank application
	 * @return 			 amount of money on the bank account if valid account
	 * 					 and '0' otherwise
	 * 
	 * @author Daniil Provornii, s192678
	 */
	public BigDecimal getBalance(String accountID) {
		try {
			Account account = bank.getAccount(accountID);
			return account.getBalance();
			
		} catch (BankServiceException_Exception e) {
			
			System.err.println("getAccount: " + e.getMessage());
			return new BigDecimal(0);
		}
	}
	
	/**
	 * Check if account with such id exists in the bank DB
	 *
	 * @param accountID	 id of the user in the bank application
	 * @return 			 true if account with this is exists
	 * 					 false otherwise
	 * 
	 * @author Daniil Provornii, s192678
	 */
	public boolean accountExists(String accountID) {
		try {
			if(bank.getAccount(accountID) != null) {
				return true;
			}
			
		} catch (BankServiceException_Exception e) {}
		
		return false;
	}
}
