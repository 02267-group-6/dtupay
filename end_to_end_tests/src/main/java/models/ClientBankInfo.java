package models;

/**
 * Model to represent payload for a request to create user
 * It needs to send username, bankaccountId and the userType
 * customer or merchant
 *
 * @author Daniil Provornii, s192678
 */
public class ClientBankInfo {
    protected String name;
    protected String bankId;
	protected String userType;

    
    public String getName() {
        return name;
    }
    
    public void setName(String value) {
        this.name = value;
    }
    
    public String getBankId() {
        return bankId;
    }
    
    public void setBankId(String value) {
        this.bankId = value;
    }
    
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
}
