package models;

/**
 * Model to deserialize the reponse when requesting a token
 *
 * @author Daniil Provornii, s192678, Ergys Kajo s181412, Hussain Tariq Awana s181434
 */
public class TokenResponse {
	protected String[] tokens;
	
	public String[] getTokens() {
		return tokens;
	}
	public void setTokens(String[] tokens) {
		this.tokens = tokens;
	}
}
