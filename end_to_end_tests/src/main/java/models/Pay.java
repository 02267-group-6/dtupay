package models;

import java.math.BigDecimal;

/**
 * Model to represent payload for a request to pay
 * It needs to send amount of money, a valid token and merchant ID
 *
 * @author Daniil Provornii, s192678
 */
public class Pay {
	
	protected BigDecimal amount;
	protected String token;
	protected String merchantID;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal value) {
		this.amount = value;
	}

	public void setMerchantId(String value) {
		this.merchantID = value;
	}

	public String getMerchantId() {
		return merchantID;
	}

	public void setToken(String value) {
	    this.token = value;
	}
	    
    public String getToken() {
        return token;
    }
}
