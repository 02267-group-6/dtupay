package database;

import java.util.ArrayList;
import java.util.List;
import models.User;

/**
 * In-memory database for the users
 * 
 *
 * @author Jeff Gyldenbrand, s202790, Daniil Provornii s192678 and Christian Bruun Nielsen, s153867
 */
public class UserDB {
	
	public static ArrayList<User> db = new ArrayList<User>();
	private static UserDB instance = null;


	private UserDB() {
	}

	public static UserDB UserDB() {

		if (instance == null)
			instance = new UserDB();

		return instance;

	}

	public int UserCount() {
		return db.size();
	}


	public void unregisterUser(String id) {
		int intID = Integer.parseInt(id);

		db.remove(intID);
		
	}

	public void updateUser(String id, User user) {
		int intID = Integer.parseInt(id);

		db.set(intID, user);
		
	}

	public void insertUserToDatabase(User c) {
		String id = String.valueOf(db.size());
		c.setId(id);

		db.add(c);
		
	}

	public List<User> findUserByName(String name) {
		ArrayList<User> newList = new ArrayList<User>();

		for (int i = 0; i < db.size(); i++) {
			if (name.equals(db.get(i).getName())) {
				newList.add(db.get(i));
			}
		}

		return newList;
	}

	public User findUser(String id) {
		return db.get(Integer.parseInt(id));
	}
	
	public boolean isUserValid(String id) {

		boolean isValid = false;

		for (User m : db) {
			if (m.getId().equals(id)) {
				isValid = true;
			}
		}

		return isValid;
	}

	public void clear() {
		db.clear();
	}
}
