package resources.users;


import database.UserDB;
import logic.DatabaseHandler;
import logic.DatabaseHandlerFactory;
import models.ClientBankInfo;

import models.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Endpoint:
 * GET: gets a list of users based on the a given name.
 * POST: registers a new usders and returns its id.
 *
 * @author Jeff Gyldenbrand, s202790 and Daniil Provornii, s192678 and Antoine, s
 */
@Path("users")
public class Users {

//	DatabaseHandler databaseHandler = new DatabaseHandler();
	DatabaseHandler service = new DatabaseHandlerFactory().getService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUser(@QueryParam("name") String name) {
		try {
			List<User> result = service.getUserByName(name);

			if (result.isEmpty()) {
				throw new Exception("No merchant '" + name + "' was found");
			} else {
				return service.getUserByName(name);
			}


		} catch (Exception e) {
//			throw new NotFoundException(name + ": not found: " + e);
//			return Response.ok(String.valueOf(id), MediaType.TEXT_PLAIN).build();
			return null;
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerUser(ClientBankInfo info) {
		
		try {
			int id = service.registerUser(info);
	
			UserDB db = UserDB.UserDB();
			return Response.ok(String.valueOf(id), MediaType.TEXT_PLAIN).build();

		} catch (Exception e) {
			return Response.ok(e.getMessage(), MediaType.TEXT_PLAIN).build();
		}
		//db.printDatabase();

	}

}
