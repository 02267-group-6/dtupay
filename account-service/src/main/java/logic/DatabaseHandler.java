package logic;

import database.UserDB;
import dtu.ws.fastmoney.Account;
import messaging.Event;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import messaging.EventReceiver;
import models.ClientBankInfo;
import models.User;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Handles registrations, updates and deletions of customers and merchants
 * in the DTUPay database.
 *
 * @author Jeff Gyldenbrand, s202790, Daniil Provornii s192678
 */


public class DatabaseHandler implements EventReceiver {

	UserDB userDatabase = UserDB.UserDB();
	private CompletableFuture<Integer> result;
	private EventSender eventSender;
	
	public DatabaseHandler(EventSender eventSender) {
        this.eventSender = eventSender;
   }

	/**
	 * Find a customer from its id.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public User getUser(String id) {
		return userDatabase.findUser(id);
	}

	/**
	 * Finds and returns a list of customers with the given name.
	 *
	 * @param name customer first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<User> getUserByName(String name) {
		return userDatabase.findUserByName(name);
	}


	/**
	 * Registration of new customer.
	 * This function send a rabbitMq message to payment to register a customer/merchant
	 *
	 * @param info consist of customers name and bank identification.
	 * @author Jeff Gyldenbrand, s202790, Daniil Provornii s192678
	 */
	public int registerUser(ClientBankInfo info) {

		try {

			User user = new User();
			user.setName(info.getName());
			user.setBankId(info.getBankId());


			userDatabase.insertUserToDatabase(user);
			
			Event event = null;
			
			if(info.getUserType().equals("customer")) {
				event = new Event("create_customer", new String[] { user.getName(), user.getBankId() });
				
			} else {
				event = new Event("create_merchant", new String[] { user.getName(), user.getBankId() });
				
			}
			
			result = new CompletableFuture<>();
			eventSender.sendEvent(event);
			

			return result.join();

		} catch (Exception e) {
			throw new NotFoundException(e);
		}

	}


	/**
	 * Updates existing customers information
	 *
	 * @param id       customers DTUPay identification.
	 * @param customer customer object.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public User updateUser(String id, User user) {
		userDatabase.updateUser(id, user);
		return userDatabase.db.get(Integer.parseInt(id));
	}


	/**
	 * Removes a customer from the database.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void unregisterUser(String id) {
		userDatabase.unregisterUser(id);
	}


	
	@Override
	public void receiveEvent(Event event) {
	    switch(event.getEventType()) {
	        case "user_created":
				System.out.println("user_created: " + event);
				result.complete(Integer.parseInt(String.valueOf(event.getArguments()[0])));

	            break;
	        default:
	            System.out.println("event ignored: " + event);
	    }
	}



}
