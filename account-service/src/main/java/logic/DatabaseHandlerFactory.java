package logic;

import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

/**
 * Creates a singleton service and initializes RabbitMq
 * 
 *
 * @author Jeff Gyldenbrand, s202790, Daniil Provornii s192678
 */
public class DatabaseHandlerFactory {
    static DatabaseHandler service = null;

    public DatabaseHandler getService() {
        // The singleton pattern.
        // Ensure that there is at most one instance of a PaymentService
        if (service != null)
            return service;


        EventSender b = new RabbitMqSender();
        service = new DatabaseHandler(b);
        RabbitMqListener r = new RabbitMqListener(service);
        try {
            r.listen();
        } catch (Exception e) {
            throw new Error(e);
        }
        return service;
    }
}

