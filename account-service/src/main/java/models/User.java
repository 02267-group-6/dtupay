package models;

/**
 * User model to store generic information about user profile
 * in the DTUPay database.
 *
 * @author Jeff Gyldenbrand, s202790, Daniil Provornii s192678
 */
public class User {
	
	private String id;//mid
	private String name;
	private int cash;// type can be double and amount
	private String bankId;
	
	public User() {

	}

	public User(String id, String name, int cash, String bankId) {
		this.id = id;
		this.name = name;
		this.cash = cash;
		this.bankId = bankId;
	}


	public String getUser(String id) {
		return id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}


	public String getBankId() {
		return bankId;
	}

	public void setBankId(String id) {
		this.bankId = id;
	}

}
