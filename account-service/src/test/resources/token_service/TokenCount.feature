# author: Antoine Sébert s193508

Feature: Token Manager get count

	# Correct behavior

	Scenario: get the token count for an existing customer
		Given a customer with id "0" exists and has 4 tokens
		Then the token count for the customer with id "0" is 4.

	# Incorrect behavior

	Scenario: get the token count for a non-existing user
		Given there exists no customer with id "1"
		Then the token count for the customer "1" produces a CustomerNotFound exception.