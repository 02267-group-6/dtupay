package accounts_service;

import messaging.EventReceiver;
import messaging.EventSender;
import messaging.Event;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.math.BigDecimal;
import org.junit.jupiter.api.Assertions;
import database.UserDB;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankServiceException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import logic.DatabaseHandler;
import models.ClientBankInfo;
import models.User;
import java.util.concurrent.CompletableFuture;

/**
 * Cucumber Tests for AccountHandler
 *
 * @author Christian Bruun Nielsen, s153867 and Jeff Gyldenbrand, s202790
 */

public class DTUPayAccountsSteps {
	
	// Setup of variables
	private CompletableFuture<Boolean> result;
	private EventSender eventSender;
	private UserDB userDB = UserDB.UserDB();
	private DatabaseHandler databaseHandler = new DatabaseHandler(eventSender);
	private ClientBankInfo cbi_customer = new ClientBankInfo();
	private ClientBankInfo cbi_merchant = new ClientBankInfo();
	private Account customerAccount = new Account();
	private Account merchantAccount = new Account();
	private String name;

/*
	// CUSTOMER TESTS
	
	// Create Customer Test
	
	// Find Customer using given bank account ID 
	@Given("the customer has a bank account id {string}")
	public void the_customer_has_a_bank_account_id(String string) {
		// Clears database for testing purposes
		userDB.clear();
		// Get the account from Bank using CPR as ID 
		// but due to these test not being able to communicate with the bank
		// the cbi_customer is populated using information from the existing bank account
		cbi_customer.setBankId("42ec4b47-bee2-45a9-9ab5-eb55531401ee");
		cbi_customer.setName("Jeff");
	}
	
	// Register the account to DTUPay user database
	@When("customer creates the DTUPay account")
	public void customer_creates_the_dtu_pay_account() {
		// Since the user being added is a customer, this is being marked
		cbi_customer.setUserType("customer");
		// Adds the account to database
		databaseHandler.registerUser(cbi_customer);
	}
	
	// Check to find the account in the database
	@Then("the response is the account id {string}, the bank id is {string} and the name is {string}")
	public void checkAccountInfo(String id, String bankId, String name) {
		// Since the database was just cleared, the only entry in the database 
		// should be the newly added customer, hence index 0  
	    // Check if the found user was the customer added
		assertEquals(id, UserDB.db.get(0).getId());	
		// Find the bank ID using the customer account recieved from the bank
		// Check if the given ID is the same as the found
		assertEquals(bankId,customerAccount.getId());
		// Check if the given name is the same as the one in the database
		assertEquals(name, UserDB.db.get(0).getName());
	}

	
	// Update User's name
	
	// Update a user's name
	@Given("the user's id is {string} and updating the name to {string}")
	public void theCustomersIdIsAndUpdatingTheNameTo(String id, String name2) {
	    // First the user is found and imported from the database 
		User u = UserDB.UserDB().findUser(id);
		// The name of this user is changed and stored in the database
		u.setName(name2);
		userDB.updateUser(id, u);
		// Check if the correct name is stored
		assertEquals(name2, u.getName());
	}
	
	
	@When("searching on the user by his id {string}")
	public void searchingOnTheCustomerByHisId(String string) {
	// Find the name in the database	
	name = UserDB.db.get(0).getName();
	}
	
	@Then("the name {string} is returned")
	public void theNameIsReturned(String string) {
		// Check the name from the database
		assertEquals(string, name);
	}
	

	// Delete User Test
	
	@When("deleting the user with id {string}")
	public void deletingTheCustomerWithId(String string) {
		// The user is unregistered using their ID
		UserDB.UserDB().unregisterUser(string);
	}

	@Then("on search for the user with id {string} the return error should be {string}")
	public void onSearchForTheCustomerWithIdTheReturnErrorShouldBe(String id, String errorMessage) {
		// Check if the user no longer is in the database
		String result = userDB.db.get(Integer.valueOf(id)).getName();
		assertEquals(errorMessage, result);
	}
	
	// Multiple Account Test
	
	// Print Database
	@Given("the count is {string}")
	public void theCountIs(String string) {
		// Counting entries in database and printing them
		*for(int i = 0; i < CustomerDB.CustomerDB().CustomerCount(); i++) {
		*	//CustomerDB.CustomerDB();
		*	System.out.print("count " + i + ": " + CustomerDB.db.get(i).getName());
		}
		// Check if count is the same
		assertEquals(string, String.valueOf(UserDB.UserDB().UserCount()));
	}
	
	// Creation and registration of two users, a customer and a merchant
	@When("adding two new users")
	public void addingNewUsers() {
		// Populating the merchant
		cbi_merchant.setName("An");
		cbi_merchant.setBankId("42ec4b47-bee2-45a9-9ab5-eb55531401ee");
		cbi_merchant.setUserType("merchant");
		// Adding merchant and adding the customer from before again
		databaseHandler.registerUser(cbi_customer);
		databaseHandler.registerUser(cbi_merchant);
	}
	
	// Recount of database
	@Given("the count is now {string}")
	public void theCountIsNow(String string) {
		// Counting entries in database and printing them
		for(int i = 0; i < CustomerDB.CustomerDB().CustomerCount(); i++) {
			//CustomerDB.CustomerDB();
			System.out.print("count " + i + ": " + CustomerDB.db.get(i).getName());
		}
		// Check if count is the same
		assertEquals(string, String.valueOf(UserDB.UserDB().UserCount()));
	}


	*/
}
