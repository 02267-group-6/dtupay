package accounts_service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;

import database.CustomerDB;
import database.MerchantDB;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankServiceException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import logic.BankHandler;
import logic.DatabaseHandler;
import models.ClientBankInfo;
import models.Customer;
//import token_service.logic.CustomerNotFound;

public class DTUPayAccountsSteps {
	
	private static CustomerDB customerDb = CustomerDB.CustomerDB();
//	private static MerchantDB merchantDb = MerchantDB.MerchantDB();
	private static BankHandler bankHandler = new BankHandler();
	private static DatabaseHandler databaseHandler = new DatabaseHandler();
	
	private static ClientBankInfo cbi_customer = new ClientBankInfo();
	private static ClientBankInfo cbi_merchant = new ClientBankInfo();
	private static Account customerAccount = new Account();
	private static Account merchantAccount = new Account();
	private String name;
	
	@Given("the customer has a bank account id: {string}")
	public void the_customer_has_a_bank_account_id(String string) {
	customerDb.clear();
		
	
		customerAccount = bankHandler.getAccountByCPR(string);
		
		cbi_customer.setBankId(customerAccount.getId());
		cbi_customer.setName(customerAccount.getUser().getFirstName());


		
	}

	@When("customer creates the DTUPay account")
	public void customer_creates_the_dtu_pay_account() {
		
		databaseHandler.registerCustomer(cbi_customer);
	    
	}

	@Then("the response is the account id: {string}")
	public void the_response_is_the_account_id(String string) {
		
	    String result = CustomerDB.db.get(0).getId();    

	
		
		assertEquals(string, result);
		
		
	}
	
	@Then("the bank id is: {string}")
	public void the_bank_id_is(String string) {
		String bankId = customerAccount.getId();
		
		assertEquals(string, bankId);
	}
	
	@Then("the name is: {string}")
	public void theNameIs(String string) {
		assertEquals(string, CustomerDB.db.get(0).getName());
	}
	
	
	

	
	
	@Given("the customers id is {string} and updating the name to: {string}")
	public void theCustomersIdIsAndUpdatingTheNameTo(String id, String name2) {
	    
		Customer c = CustomerDB.CustomerDB().findCustomer(id);

		c.setName(name2);
		
		
		
		customerDb.updateCustomer(id, c);
		
		
		assertEquals(name2, c.getName());
		
		
	}
	
	
	@When("searching on the customer by his id {string}")
	public void searchingOnTheCustomerByHisId(String string) {
	  
	name = CustomerDB.db.get(0).getName();
	
	
	}
	
	@Then("the name {string} is returned")
	public void theNameIsReturned(String string) {
		assertEquals(string, name);
	}
	
	
	
	
	
	
	
	@When("deleting the customer with id {string}")
	public void deletingTheCustomerWithId(String string) {
		
	    CustomerDB.CustomerDB().unregisterCustomer(string);
	}

	@Then("on search for the customer with id {string} the return error should be {string}")
	public void onSearchForTheCustomerWithIdTheReturnErrorShouldBe(String id, String errorMessage) {
	    
	//	String name = CustomerDB.db.get(Integer.valueOf(id)).getName();
		String result = customerDb.db.get(Integer.valueOf(id)).getName();
		assertEquals(errorMessage, result);
		
	}
	

		
	
	
	/*
	////////////// Merchant
	
	@Given("the merchant has a bank account id: {string}")
	public void theMerchantHasABankAccountId(String string) {
		

		 merchantAccount = bankHandler.getAccountByCPR(string);
		
		cbi_merchant.setBankId(merchantAccount.getId());
		cbi_merchant.setName(merchantAccount.getUser().getFirstName());

	    
	}
	

	@When("merchant creates the DTUPay account")
	public void merchantCreatesTheDTUPayAccount() {
		

		databaseHandler.registerMerchant(cbi_merchant);
	    
	}
	
	@Then("the response is the merchants account id: {string}")
	public void the_response_is_the_merchants_account_id (String string) {
		
	    String result = MerchantDB.db.get(0).getId();    
	
		
		assertEquals(string, result);
		
		
	}
	
	@Then("the bank id of the merchant is: {string}")
	public void the_bank_id_of_the_merchant_is(String string) {
		String bankId = merchantAccount.getId();
		
		assertEquals(string, bankId);
	}
*/
	
	
	// print database
	
	@Given("the count is {string}")
	public void theCountIs(String string) {
		
		for(int i = 0; i < CustomerDB.CustomerDB().CustomerCount(); i++) {
			//CustomerDB.CustomerDB();
			System.out.print("count " + i + ": " + CustomerDB.db.get(i).getName());
		}
		
		assertEquals(string, String.valueOf(CustomerDB.CustomerDB().CustomerCount()));
	}
	
	
	




}
