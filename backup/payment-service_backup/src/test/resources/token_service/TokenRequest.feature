# author: Antoine Sébert s193508

Feature: Token Manager token request

	# Correct behavior

	Scenario: The Customer requests [1; 5] tokens and has [0; 1] unused tokens
		Given the customer with id "1" exists and has 0 tokens
		And the customer "1" requests 2 tokens
		Then the customer "1" has 2 tokens.

	# Incorrect behavior
	Scenario: The Customer requests tokens but has more than 1 unused tokens left.
		Given the customer with id "2" exists and has 2 tokens
		And the customer "2" requests 4 tokens
		Then the token request for the customer "2" produces a TooManyUnusedTokens exception.

	Scenario: The Customer requests an invalid amount of tokens.
		Given the customer with id "3" exists and has 1 tokens
		And the customer "3" requests 6 tokens
		Then the token request for the customer "3" produces a InvalidTokenAmount exception.

	Scenario: The Customer makes a request for a Customer that does not exist.
		Given the customer with id "4" does not exists
		And the customer "4" requests 2 tokens
		Then the token request for the customer "4" produces a CustomerNotFound exception.
