# author: Jeff Gyldenbrand, s202790
#
# these scenaries depends on customers already has bank accounts.

Feature: Create, update and delete a customers and merchants in DTUPay

	# create customer
	Scenario: Create a customer at DTUPay
		Given the customer has a bank account id: "221190-1355"
		When customer creates the DTUPay account
		Then the response is the account id: "0"
		And the bank id is: "42ec4b47-bee2-45a9-9ab5-eb55531401ee"
		And the name is: "Jeff"

	# update his name
	Scenario: Update the customers name
		Given the customers id is "0" and updating the name to: "bob"
		When searching on the customer by his id "0"
		Then the name "bob" is returned
		
	Scenario: Delete the customer
		When deleting the customer with id "0"
		Then on search for the customer with id "0" the return error should be "deleted customer"
		


		


