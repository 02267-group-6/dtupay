package resources.account;

import database.MerchantDB;
import logic.DatabaseHandler;
import models.ClientBankInfo;
import models.Merchant;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Endpoint:
 * GET: gets a list of merchants based on the a given name.
 * POST: registers a new merchant and returns its id.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("merchants")
public class Merchants {

	DatabaseHandler cr = new DatabaseHandler();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Merchant> getMerchant(@QueryParam("name") String name) {
		try {
			List<Merchant> result = cr.getMerchantByName(name);

			if (result.isEmpty()) {
				throw new Exception("No merchant '" + name + "' was found");
			} else {
				return cr.getMerchantByName(name);
			}


		} catch (Exception e) {
			throw new NotFoundException(name + ": not found: " + e);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerMerchant(ClientBankInfo info) throws URISyntaxException {

		int id = cr.registerMerchant(info);

		MerchantDB db = MerchantDB.MerchantDB();
		db.printDatabase();

		return Response.ok(String.valueOf(id), MediaType.TEXT_PLAIN).build();

	}

}
