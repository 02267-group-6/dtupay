package resources.account;

import database.CustomerDB;
import logic.DatabaseHandler;
import models.ClientBankInfo;
import models.Customer;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Endpoint:
 * GET: gets a list of customers based on the a given name.
 * POST: registers a new customer and returns its id.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("customers")
public class CustomersResource {

	DatabaseHandler cr = new DatabaseHandler();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> getCustomer(@QueryParam("name") String name) {
		try {
			List<Customer> result = cr.getCustomerByName(name);

			if (result.isEmpty()) {
				throw new Exception("No users '" + name + "' was found");
			} else {
				return cr.getCustomerByName(name);
			}


		} catch (Exception e) {
			throw new NotFoundException(name + ": not found: " + e);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerCustomer(ClientBankInfo info) throws URISyntaxException {
		int id = cr.registerCustomer(info);

		CustomerDB db = CustomerDB.CustomerDB();
		db.printDatabase();

		return Response.ok(String.valueOf(id), MediaType.TEXT_PLAIN).build();

		//return Response.created(new URI("customer/" + id)).build();
	}
}
