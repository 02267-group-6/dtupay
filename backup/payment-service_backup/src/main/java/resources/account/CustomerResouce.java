package resources.account;

import logic.DatabaseHandler;
import models.Customer;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Endpoint:
 * GET: gets a customer given its id.
 * PUT: updates the customer given its id and a customer.
 * DELETE: removes a customer from the database.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("customer/{id}")
public class CustomerResouce {
	DatabaseHandler cr = new DatabaseHandler();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Customer getCustomer(@PathParam("id") String id) {
		return cr.getCustomer(id);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Customer putCustomer(@PathParam("id") String id, Customer customer) {
		return cr.updateCustomer(id, customer);
	}

	@DELETE
	public void deleteCustomer(@PathParam("id") String id) {
		cr.unregisterCustomer(id);
	}

}
