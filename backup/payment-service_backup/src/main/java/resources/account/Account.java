package resources.account;

import logic.BankHandler;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Endpoint:
 * GET: gets the bank account from provided bank id.
 * DELETE: retires the bank account.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("account/{id}")
public class Account {

	// TODO: move bank logic into bank handler
	BankHandler acc = new BankHandler();


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public dtu.ws.fastmoney.Account getCAccount(@PathParam("id") String id) {
		return acc.getAccount(id);

	}

	@DELETE
	public void deleteCustomer(@PathParam("id") String id) {
		acc.deleteAccount(id);
	}

}
