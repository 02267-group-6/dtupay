package resources.account;

import logic.BankHandler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Endpoint:
 * GET: gets the bank account from provided CPR number.
 *
 * @author Jeff Gyldenbrand, s202790
 */
@Path("account/CPR/{id}")
public class AccountCPR {

	BankHandler acc = new BankHandler();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public dtu.ws.fastmoney.Account getCAccount(@PathParam("id") String id) {
		return acc.getAccountByCPR(id);

	}
}
