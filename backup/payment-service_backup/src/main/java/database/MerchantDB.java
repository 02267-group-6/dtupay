package database;

import models.Merchant;

import java.util.ArrayList;
import java.util.List;

/**
 * Pseudo database to handle merchants in DTUPay microservice.
 *
 * @author Jeff Gyldenvrand, s202790
 */
public class MerchantDB {

	public static ArrayList<Merchant> db = new ArrayList<Merchant>();
	static MerchantDB merchant = new MerchantDB();
	private static MerchantDB instance = null;


	private MerchantDB() {
	}

	public static MerchantDB MerchantDB() {

		if (instance == null)
			instance = new MerchantDB();

		return instance;

	}

	/**
	 * Helper-function for debugging:
	 * Prints all merchants from the database.
	 *
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void printDatabase() {

		System.out.println("\nDTUPay Merchant Database:");
		for (int i = 0; i < db.size(); i++) {
			System.out.println(
					"----------\n" +
							"id: " + db.get(i).getId() + "\n" +
							"name: " + db.get(i).getName() + "\n" +
							"cash: " + db.get(i).getCash() + "\n"
			);
		}

	}

	/**
	 * Finds and returns a list of merchants with the given name.
	 *
	 * @param name merchants first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<Merchant> findMerchantByName(String name) {

		ArrayList<Merchant> newList = new ArrayList<Merchant>();

		for (int i = 0; i < db.size(); i++) {
			if (name.equals(db.get(i).getName())) {
				newList.add(db.get(i));
			}
		}

		return newList;

	}


	/**
	 * Removes a merchant from the database.
	 *
	 * @param id merchants DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void unregisterMerchant(String id) {
		int intID = Integer.parseInt(id);

		db.remove(intID);

	}

	/**
	 * Checks if the merchant exists in the database
	 *
	 * @param id merchant DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public boolean isMerchantValid(String id) {

		boolean isValid = false;

		for (Merchant m : db) {
			if (m.getId().equals(id)) {
				isValid = true;
			}
		}

		return isValid;
	}

	/**
	 * Finds and returns a merchant from the database.
	 *
	 * @param id merchant DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Merchant findMerchant(String id) {

		return db.get(Integer.parseInt(id));
	}


	/**
	 * Returns the bank identifier of a merchant.
	 *
	 * @param id merchant DTUPay identifier.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public String getMerchantBankId(String id) {

		int index = Integer.valueOf(id);

		return db.get(Integer.valueOf(id)).getBankId();
	}


	/**
	 * Inserts a new merchant into database.
	 * The merchant identification is based on its index in the database.
	 *
	 * @param name merchant first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void insertMerchantToDatabase(Merchant c) {

		String id = String.valueOf(db.size());
		c.setId(id);

		db.add(c);
	}


}
