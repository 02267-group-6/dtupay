package database;

import models.Customer;
import models.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Pseudo database to handle customers in DTUPay micro-service.
 *
 * @author Jeff Gyldenvrand, s202790
 */
public class CustomerDB {
	public static ArrayList<Customer> db = new ArrayList<Customer>();
	static Customer customer = new Customer();
	private static CustomerDB instance = null;

	private CustomerDB() {
	}

	public static CustomerDB CustomerDB() {
		if (instance == null)
			instance = new CustomerDB();

		return instance;
	}

	/**
	 * Helper-function for debugging:
	 * Prints all customers from the database.
	 *
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void printDatabase() {
		System.out.println("Count: " + db.size());
		System.out.println("DTUPay customers Database:");
		for (int i = 0; i < db.size(); i++) {
			System.out.println(
					"----------\n" +
							"id: " + db.get(i).getId() + "\n" +
							"name: " + db.get(i).getName() + "\n" +
							"cash: " + db.get(i).getCash() + "\n" +
							"tokens: " + db.get(i).getTokenCount()
			);
		}
	}

	/**
	 * Returns the amount of tokens a customer currently has.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public int tokenCount(String id) {
		return db.get(Integer.parseInt(id)).getTokenCount();
	}
	
	public int CustomerCount() {
		return db.size();
	}

	/**
	 * Add a list of tokens to a customer's list of tokens.
	 *
	 * @param id     a Customer ID
	 * @param tokens a list of tokens
	 * @author Antoine Sébert s193508
	 */
	public void addTokens(String id, List<Token> tokens) {
		findCustomer(id).addTokens(tokens);
	}

	/**
	 * Removes a token from a customer's list of tokens.
	 *
	 * @param id    a Customer ID
	 * @param token a token
	 */
	public void removeToken(String id, Token token) {
		findCustomer(id).removeToken(token);
	}

	/**
	 * Updates and returns a customer.
	 *
	 * @param id customers DTUPay identification.
	 * @param c  customer object.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void updateCustomer(String id, Customer c) {
		int intID = Integer.parseInt(id);

		db.set(intID, c);

		//return db.get(intID);
	}

	/**
	 * Clears the database.
	 *
	 * @author Antoine Sébert s193508
	 */
	public void clear() {
		db.clear();
	}


	/**
	 * Removes a customer from the database.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void unregisterCustomer(String id) {

		// Hacky solution until better one is developed:
		// Customers don't really get deleted, just overwritten right now,
		// to keep the ids to match their respective index places in the database.
		Customer c = new Customer();
		c.setBankId("0");
		c.setCash(0);
		c.setName("deleted customer");

		db.set(Integer.parseInt(id), c);
		//db.remove(Integer.parseInt(id));
	}

	/**
	 * Finds and returns a customer from the database.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Customer findCustomer(String id) {

		return db.get(Integer.parseInt(id));
	}

	/**
	 * Finds customer DTUPay identification from token id.
	 *
	 * @param token token identifier.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public String findCustomerToken(String token) {
		String customerID = "";

		for (Customer c : db)
			if (c.hasToken(token)) {
				customerID = c.getId();
				break;
			}

		return customerID;
	}

	/**
	 * Checks if the customer exists in the database
	 *
	 * @param id customer DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public boolean isCustomerValid(String id) {
		boolean isValid = false;

		for (Customer c : db)
			if (c.getId().equals(id)) {
				isValid = true;
				break;
			}

		return isValid;
	}

	/**
	 * Finds and returns a list of customers with the given name.
	 *
	 * @param name customer first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<Customer> findCustomerByName(String name) {
		ArrayList<Customer> newList = new ArrayList<Customer>();

		for (int i = 0; i < db.size(); i++) {
			if (name.equals(db.get(i).getName())) {
				newList.add(db.get(i));
			}
		}

		return newList;
	}

	/**
	 * Inserts a new customer into database.
	 * The customers identification is based on its index in the database.
	 *
	 * @param name customer first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void insertCustomerToDatabase(Customer c) {

		String id = String.valueOf(db.size());
		c.setId(id);

		db.add(c);
	}

	/**
	 * Returns the bank identifier of a customer.
	 *
	 * @param id customers DTUPay identifier.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public String getCustomerBankId(String id) {
		return db.get(Integer.valueOf(id)).getBankId();
	}

	///////// THIS IS FOR TESTING PURPOSES, DELETE IT BEFORE DELIVERY
	public String getFirstToken() {
		return db.get(0).getFirstToken();
	}
	///////////////////

}
