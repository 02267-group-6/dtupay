package logic;

import database.CustomerDB;
import database.MerchantDB;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import models.ClientBankInfo;
import models.Customer;
import models.Merchant;

import javax.ws.rs.NotFoundException;
import java.util.List;

/**
 * Handles registrations, updates and deletions of customers and merchants
 * in the DTUPay database.
 *
 * @author Jeff Gyldenbrand, s202790
 */
public class DatabaseHandler {

	CustomerDB customerDatabase = CustomerDB.CustomerDB();
	MerchantDB merchantDatabase = MerchantDB.MerchantDB();
	BankService bank = new BankServiceService().getBankServicePort();


	/**
	 * Find a customer from its id.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Customer getCustomer(String id) {

		return customerDatabase.findCustomer(id);
	}

	/**
	 * Finds and returns a list of customers with the given name.
	 *
	 * @param name customer first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<Customer> getCustomerByName(String name) {
		return customerDatabase.findCustomerByName(name);
	}


	/**
	 * Registration of new customer.
	 * This function contacts the bank to retrieve the customers bank id.
	 *
	 * @param info consist of customers name and bank identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public int registerCustomer(ClientBankInfo info) {

		Account account = new Account();

		try {

			account = bank.getAccount(info.getBankId());
			Customer c = new Customer();
			c.setName(info.getName());
			c.setBankId(info.getBankId());


			customerDatabase.insertCustomerToDatabase(c);
			return Integer.parseInt(CustomerDB.db.get(CustomerDB.db.size() - 1).getId());

		} catch (Exception e) {
			throw new NotFoundException(e);
		}

	}


	/**
	 * Updates existing customers information
	 *
	 * @param id       customers DTUPay identification.
	 * @param customer customer object.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Customer updateCustomer(String id, Customer customer) {
		customerDatabase.updateCustomer(id, customer);
		return CustomerDB.db.get(Integer.parseInt(id));
	}


	/**
	 * Removes a customer from the database.
	 *
	 * @param id customers DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void unregisterCustomer(String id) {
		customerDatabase.unregisterCustomer(id);
	}


	/**
	 * Finds a merchant from its id.
	 *
	 * @param id merchant DTUPay identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Merchant getMerchant(String id) {

		return merchantDatabase.findMerchant(id);
	}


	/**
	 * Registration of new merchant.
	 * This function contacts the bank to retrieve the merchants bank id.
	 *
	 * @param info consist of merchants name and bank identification.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public int registerMerchant(ClientBankInfo info) {

		Account account = new Account();

		try {

			account = bank.getAccount(info.getBankId());
			Merchant m = new Merchant();
			m.setName(info.getName());
			m.setBankId(info.getBankId());

			merchantDatabase.insertMerchantToDatabase(m);

			return Integer.parseInt(MerchantDB.db.get(MerchantDB.db.size() - 1).getId());

		} catch (Exception e) {
			throw new NotFoundException(e);
		}

	}

	/**
	 * Finds and returns a list of merchants with the given name.
	 *
	 * @param name merchants first name.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<Merchant> getMerchantByName(String name) {
		return merchantDatabase.findMerchantByName(name);
	}

}
