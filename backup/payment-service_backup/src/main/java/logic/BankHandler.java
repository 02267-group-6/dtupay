package logic;

import database.CustomerDB;
import database.MerchantDB;
import dtu.ws.fastmoney.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Handles all interactions between users of DTUPay and the bank.
 *
 * @author Jeff Gyldenbrand, s202790
 */
public class BankHandler {

	BankService bank = new BankServiceService().getBankServicePort();

	CustomerDB customerDatabase = CustomerDB.CustomerDB();
	MerchantDB merchantDatabase = MerchantDB.MerchantDB();


	/**
	 * Handles money transfers between customer and merchant at the bank.
	 *
	 * @param token      a customers unique token
	 * @param merchantId the DTUPay database identification of the merchant
	 * @param amount     amount of money
	 * @author Jeff Gyldenbrand, s202790
	 */
	public String pay(String token, String merchantId, BigDecimal amount) {

		String ret = "some error has occured";

		// find debitor DTUPay and bank identification from given token.
		String debitor = customerDatabase.findCustomerToken(token);
		String debitorBankId = customerDatabase.getCustomerBankId(debitor);

		// find creditor bank identification 
		String creditorBankId = merchantDatabase.getMerchantBankId(merchantId);

		// validate that customer and merchant exists in DTUPay databases
		boolean merchantIsValid = merchantDatabase.isMerchantValid(merchantId);
		boolean customerIsValid = customerDatabase.isCustomerValid(debitor);

		if (merchantIsValid && customerIsValid && !debitor.isEmpty()) {
			try {

				bank.transferMoneyFromTo(debitorBankId, creditorBankId, amount, "description");
				ret = "success";

			} catch (BankServiceException_Exception e) {

				ret = "not enough money";
				//throw new Error("Pay: "  + e.getMessage());
			}
		}
		return ret;

	}


	/**
	 * Returns all accounts from bank
	 *
	 * @author Jeff Gyldenbrand, s202790
	 */
	public List<AccountInfo> getBankAccounts() {
		return bank.getAccounts();
	}

	/**
	 * Returns specific account from bank.
	 *
	 * @param id a customers bank identifier.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Account getAccount(String id) {
		try {
			return bank.getAccount(id);
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns specific account from bank.
	 *
	 * @param id a customers CPR number.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public Account getAccountByCPR(String id) {
		try {
			return bank.getAccountByCprNumber(id);
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Deletes specific account in bank
	 *
	 * @param id a customers bank identifier.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public void deleteAccount(String id) {
		try {
			bank.retireAccount(id);
		} catch (BankServiceException_Exception e) {

			e.printStackTrace();
		}
	}

	/**
	 * creates an account in bank.
	 *
	 * @param user    Object with: CPR number, first name, last name.
	 * @param balance amount of money.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public String createAccount(User user, BigDecimal balance) {
		try {
			return bank.createAccountWithBalance(user, balance);
		} catch (BankServiceException_Exception e) {

			throw new Error("create bank account: " + e.getMessage());
		}
	}

	/**
	 * Returns a boolean that states if a customer has at most one token or not.
	 *
	 * @param id a customers DTUPay identifier.
	 * @author Jeff Gyldenbrand, s202790
	 */
	public boolean hasAtMostOneToken(String id) {
		int tokens = customerDatabase.tokenCount(id);
		if (tokens <= 1) {
			return true;
		} else {
			return false;
		}
	}


}
