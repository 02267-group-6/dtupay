package models;

import models.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Customer model used in DTUPay
 *
 * @author Jeff Gyldenbrand, s202790
 */
public class Customer {

	//public static Customer instance;

	private String id;
	private String name;
	private int cash;
	private ArrayList<Token> tokens = new ArrayList<>();//it should be an arraylist of tokens as a customer can store up to 5 valid  tokens
	private String bankId;

	public Customer() {

	}

	public Customer(String id, String name, int cash, ArrayList<Token> tokens, String bankId) {
		this.id = id;
		this.name = name;
		this.cash = cash;
		this.tokens = tokens;
		this.bankId = bankId;
	}

	/**
	 * Add a list of tokens to the customer's list of tokens.
	 *
	 * @param tokens a list of tokens
	 * @author Antoine Sébert s193508
	 */
	public void addTokens(List<Token> tokens) {
		this.tokens.addAll(tokens);
	}

	/**
	 * Removes a token from the customer's list of tokens.
	 *
	 * @param token a token
	 */
	public void removeToken(Token token) {
		for (Token t : tokens)
			if (t.getId() == token.getId()) {
				tokens.remove(t);
				break;
			}
	}

	///////// THIS IS FOR TESTING PURPOSES, DELETE IT BEFORE DELIVERY
	public String getFirstToken() {
		return tokens.get(0).toString();
	}
	///////////////////

	public int getTokenCount() {
		return tokens.size();
	}

	public void setTokenCount(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}

	public void printTokens() {
		System.out.println(name + "'s token ids:");
		for (Token t : tokens) {
			System.out.println(t.getId());
		}
	}

	public boolean hasToken(String token) {

		boolean foundToken = false;


		for (Token t : tokens) {
			if (t.getId().toString().equals(token)) {
				foundToken = true;
			}
		}

		return foundToken;

	}

	public String getCustomer(String id) {
		return id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String id) {
		this.bankId = id;
	}


}
