## Collaborators:
* Jeff Gyldenbrand, s202790
* Christian Bruun Nielsen, s153867
* Ergys Kajo, s181412
* Daniil Provornii, s192678
* Antoine Sébert, s193508
* Hussain Tariq Awana, s181434
<hr>

To build and run
```shell script
./build_and_run.sh
```
